import logging
import os
import shutil
import sys

from buildtools import os_utils, log

os_utils.safe_rmtree('dist')
os_utils.ensureDirExists('dist/NaniteConstructionSystem/Data/Scripts')
os_utils.copytree('Content','dist/NaniteConstructionSystem', verbose=True)
os_utils.copytree('src','dist/NaniteConstructionSystem/Data/Scripts', verbose=True, ignore=['.csproj'])
